#!/bin/bash

export NANOPB_DIR=~/nanopb
cd proto
./make.sh stm32f3discovery_msg
cp stm32f3discovery_msg_pb2.py ../example_python
cp stm32f3discovery_msg_pb2.py ../example_ros
cp stm32f3discovery_msg.pb.c ../example_c
cp stm32f3discovery_msg.pb.h ../example_c
cd ..
cd example_c
./make.sh
cd ..
