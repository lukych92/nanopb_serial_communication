#include <stdio.h>
#include <pb_encode.h>
#include <pb_decode.h>
#include "stm32f3discovery_msg.pb.h"

int main(void)
{
    printf("Simple test start\n");
    uint8_t buffer[128];
    size_t message_length;


    printf("Encoding...\n");


    bool status;

    stm32f3discovery_msg msg_encoded = stm32f3discovery_msg_init_zero;
    msg_encoded.btn_user = false;
    msg_encoded.ld3 = true;
    msg_encoded.ld4 = false;

    pb_ostream_t stream = pb_ostream_from_buffer(buffer, sizeof(buffer));  
    status = pb_encode(&stream, stm32f3discovery_msg_fields, &msg_encoded);

    message_length = stream.bytes_written;
    
    if (!status)
    {
        printf("Encoding failed: %s\n", PB_GET_ERROR(&stream));
        return 1;
    }
    else
    {
        printf("Encoding comlete; buff: %d ;size %lu\n",*buffer, message_length);
     
    }

    printf("Decoding...\n");

    bool status_decoded;

    stm32f3discovery_msg msg_decoded = stm32f3discovery_msg_init_zero;

    pb_istream_t stream_decoded = pb_istream_from_buffer(buffer, message_length);
    status_decoded = pb_decode(&stream_decoded, stm32f3discovery_msg_fields, &msg_decoded);
        
        /* Check for errors... */
    if (!status_decoded)
    {
        printf("Decoding failed: %s\n", PB_GET_ERROR(&stream));
        return 1;
    }
    else
    {
        printf("Dencoding comlete\n");
     
    }


    printf("Decoded values: %d %d %d\n",msg_decoded.btn_user,msg_decoded.ld3,msg_decoded.ld4 );

    return 0;
}
