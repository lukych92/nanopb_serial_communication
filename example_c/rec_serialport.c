#include <errno.h>
#include <fcntl.h> 
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>

#include <pb_encode.h>
#include <pb_decode.h>
#include "stm32f3discovery_msg.pb.h"

int set_interface_attribs(int fd, int speed)
{
    struct termios tty;

    if (tcgetattr(fd, &tty) < 0) {
        printf("Error from tcgetattr: %s\n", strerror(errno));
        return -1;
    }

    cfsetospeed(&tty, (speed_t)speed);
    cfsetispeed(&tty, (speed_t)speed);

    tty.c_cflag |= (CLOCAL | CREAD);    /* ignore modem controls */
    tty.c_cflag &= ~CSIZE;
    tty.c_cflag |= CS8;         /* 8-bit characters */
    tty.c_cflag &= ~PARENB;     /* no parity bit */
    tty.c_cflag &= ~CSTOPB;     /* only need 1 stop bit */
    tty.c_cflag &= ~CRTSCTS;    /* no hardware flowcontrol */

    /* setup for non-canonical mode */
    tty.c_iflag &= ~(IGNBRK | BRKINT | PARMRK | ISTRIP | INLCR | IGNCR | ICRNL | IXON);
    tty.c_lflag &= ~(ECHO | ECHONL | ICANON | ISIG | IEXTEN);
    tty.c_oflag &= ~OPOST;

    /* fetch bytes as they become available */
    tty.c_cc[VMIN] = 1;
    tty.c_cc[VTIME] = 1;

    if (tcsetattr(fd, TCSANOW, &tty) != 0) {
        printf("Error from tcsetattr: %s\n", strerror(errno));
        return -1;
    }
    return 0;
}

void set_mincount(int fd, int mcount)
{
    struct termios tty;

    if (tcgetattr(fd, &tty) < 0) {
        printf("Error tcgetattr: %s\n", strerror(errno));
        return;
    }

    tty.c_cc[VMIN] = mcount ? 1 : 0;
    tty.c_cc[VTIME] = 5;        /* half second timer */

    if (tcsetattr(fd, TCSANOW, &tty) < 0)
        printf("Error tcsetattr: %s\n", strerror(errno));
}


int main()
{
    char *portname = "/dev/ttyUSB0";
    int fd;

    fd = open(portname, O_RDWR | O_NOCTTY | O_SYNC);
    if (fd < 0) {
        printf("Error opening %s: %s\n", portname, strerror(errno));
        return -1;
    }
    /*baudrate 115200, 8 bits, no parity, 1 stop bit */
    set_interface_attribs(fd, B9600);
    set_mincount(fd, 1);                /* set to pure timed read */


    /* simple noncanonical input */
    do {
        uint8_t buffer[128];
        size_t rdlen;

        sleep(0.3);
        rdlen = read(fd, buffer, sizeof(buffer));

        if (rdlen > 0) {

          bool status_decoded;
          stm32f3discovery_msg msg_decoded = stm32f3discovery_msg_init_zero;

          pb_istream_t stream_decoded = pb_istream_from_buffer(buffer, rdlen);
          status_decoded = pb_decode(&stream_decoded, stm32f3discovery_msg_fields, &msg_decoded);
              
              /* Check for errors... */
          if (!status_decoded)
          {
/*              uint8_t   *p;
              for (p = buffer; rdlen-- > 0; p++)
                  printf(" 0x%x", *p);
              printf("\n");              
              printf("Decoded; buff: %x ;size %lu\n",*buffer, rdlen);
  */            printf("Decoding failed: %s\n", PB_GET_ERROR(&stream_decoded));
//              return 1;
          }
          printf("Decoded values: %d %d %d\n",msg_decoded.btn_user,msg_decoded.ld3, msg_decoded.ld4);

        } else if (rdlen < 0) {
            printf("Error from read: %lu: %s\n", rdlen, strerror(errno));
        }
        /* repeat read to get full message */
    } while (1);
}
