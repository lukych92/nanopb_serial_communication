#!/usr/bin/env python 

import serial
import stm32f3discovery_msg_pb2

if __name__ == "__main__":
	msg = stm32f3discovery_msg_pb2.stm32f3discovery_msg()

	while True:
		with serial.Serial('/dev/ttyUSB0', 9600, timeout=0.3) as ser:
			s = ser.read(128)        # read up to 20 bytes (timeout)
			msg.ParseFromString(s)
			print("Button: %s, LD3: %s, LD4: %s" % (msg.btn_user,msg.ld3,msg.ld4))
