#!/usr/bin/env python 

import time
import serial
import stm32f3discovery_msg_pb2

if __name__ == "__main__":
    msg = stm32f3discovery_msg_pb2.stm32f3discovery_msg()
    ser = serial.Serial('/dev/ttyACM3')  # open serial port
    print(ser.name)         # check which port was really used

    msg.ld3 = False
    msg.ld4 = True

    while True:
        msg.btn_user = True
        msg.ld3 = not msg.ld3
        msg.ld4 = not msg.ld4
        ser.write(msg.SerializeToString())     # write a string
        time.sleep(1)

    ser.close()             # close port

