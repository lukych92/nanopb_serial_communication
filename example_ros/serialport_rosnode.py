#!/usr/bin/env python 

import rospy
import serial

from stm32f3discovery_msg_pb2 import  stm32f3discovery_msg
from std_msgs.msg import Int8MultiArray
from std_msgs.msg import Bool

def leds_clb(msg):
	global msg_send
	msg_send.ld3 = bool(msg.data[0])
	msg_send.ld4 = bool(msg.data[1])

def node():
	global  msg_send
	rospy.init_node('nanopb_serial_communication',)
	pub = rospy.Publisher('stm32f3_button', Bool, queue_size=10)
	rospy.Subscriber('stm32f3_leds',Int8MultiArray,leds_clb)
	ser = serial.Serial('/dev/ttyUSB0', 115200)  # open serial port
	rospy.loginfo("Serial port {}".format(ser.name))  # check which port was really used

	msg_ros_send = Bool()

	msg_recv = stm32f3discovery_msg()

	msg_send = stm32f3discovery_msg()
	msg_send.btn_user = False
	msg_send.ld3 = False
	msg_send.ld4 = False

	rate = rospy.Rate(100) # 100hz
	while not rospy.is_shutdown():
		# read data from serial port and send to ros topic
		s = ser.read(10)
		msg_recv.ParseFromString(s)
		msg_ros_send.data = msg_recv.btn_user
		pub.publish(msg_ros_send)
		rospy.logdebug("Button: {}, LD3: {}, LD4: {}".format(msg_recv.btn_user,
															 msg_recv.ld3,
															 msg_recv.ld4)
					   )

		# send data to serial port
		ser.write(msg_send.SerializeToString())

		rate.sleep()

	ser.close()

if __name__ == "__main__":
	try:
		node()
	except:
		pass