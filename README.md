# Nanopb Serial Communication Example#
---
## Installation ##
1. Install Protocol Buffers

`
./install_protocol_buffers.sh
`

2. Install nanopb plugin

`
./install_nanopb.sh
`

3. Compile all

`
./make_all.sh
`

4. Run

`
./example_c/test_file
`

and you should see
`
Simple test start
Encoding...
Encoding comlete; buff: 8 ;size 6
Decoding...
Dencoding comlete
Decoded values: 0 1 0
`
---
## Files ##
* stm32f3discovery_msg.proto is a simple protocol buffer file  
* send_serialport.py, rec_serialport.py are serial encoder/decoder written in python  
* send_serialport.c, rec_serialport.c are serial encoder/decoder written in c
* serialport_rosnode.py is a simple ROS node  
* serialport_rosnode_wifi.py is a simple ROS node that communicate with stm32 via wifi
---
## STM32f3discovery board project ##
If you want to use nanopb buffer to serialize data from a microcontroller (e.g. STM32), in stm32f3discovery_uart, and stm32f3discovery_usb folders you can find project for STM32f3discovery in SW4STM32 environment. The example controll LD3 and LD4, and read user button.
