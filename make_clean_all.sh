#!/bin/bash

export NANOPB_DIR=~/nanopb
cd proto
make clean
cd ..
cd example_c
make clean
cd ..

rm example_c/stm32f3discovery_msg.pb.c
rm example_c/stm32f3discovery_msg.pb.h
rm example_python/stm32f3discovery_msg_pb2.py
