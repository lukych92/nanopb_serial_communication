#!/bin/bash

ACTUAL_DIR=`pwd`
cd ~
git clone https://github.com/nanopb/nanopb.git
cd ~/nanopb
git checkout f721a242f5e5a949bb24240ca00633d4519a9b94
cd generator/proto
make
cd $ACTUAL_DIR
